// Hint: `fetch#get` is an async function, which simulates a REST API.
// It returns an product object for a given id. If an id does not exist, 
// an error will be thrown.
import * as fetch from '../fetch.js';

const customer = { 
  id: '0002', 
  name: 'Bob', 
  vat: '19%' 
};

const order = {
  '0003': 20,
  '0005': 25,
  '0002': 15,
  '0001': 10,
};

export const invoice = async () => {
  // the following should yield the exact same results as v1
  return { 
    lineItems: [], // @TODO: complete me!
    // @TODO: complete me!
  };
};