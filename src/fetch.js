const productCatalog = [
  { id: '0001', name: 'Granit Schotter', price: 500 },
  { id: '0002', name: 'Quarzsand', price: 690 },
  { id: '0003', name: 'Rindenmulch', price: 250 },
  { id: '0004', name: 'Betonkies', price: 350 },
  { id: '0005', name: 'Basalt', price: 720 }
];

export const get = id => new Promise((resolve, reject) => setTimeout(() => {
  const found = productCatalog.find(product => product.id === id);
  if (found === undefined) {
    return reject(`Could not find product with id ${id}`);
  }

  return resolve(found);
}, 1000));