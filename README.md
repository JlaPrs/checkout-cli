# checkout-cli

In this JavaScript challenge, you will partly implement parts of a very simplified checkout process for ordering construction material, which calculates the final price of the order and displays it in tabular form on the console.

![](docs/output.gif)

Our frontend team started the implementation for two versions:

- `src/checkout/v1.js` which only uses hard-coded data
- `src/checkout/v2.js` which uses a (fake) API client to fetch product data for an order

Do you think you can you complete their implementation?

## Rules

You are not allowed to remove or change existing functions, data structures, or method signatures in any file within the `src` directory and its sub directories, or to change/remove the timout limit for the v2 test cases. 

You can of course add **as many new files and code as you want**. You're also allowed to **use any third party modules**, if you like to.

## Installation

You need a recent version of [Node.js](https://nodejs.org/en/) installed on your machine.

```bash
https://gitlab.com/schuettflix-case-studies/frontend/checkout-cli.git
cd checkout-cli
npm install
```

`npm start` will start the console application. Additionally and for more insights, you can can use `npm test` to run all existing unit test (which are currently of course failing).

## Hints

### Version 1
This version is pretty simple since it's only using hard-coded data. All product prices are _per ton_ and in _Cents_ (EUR). `qty` means _quantity_ (i.e. number of tons). The most challenging part might be the transformation from integer values to strings containing the `EUR` prices for the `de-DE` locale.

_Hint: That definitely sounds like something someone else might already have solved for you._

### Version 2
This is where the fun starts. Instead of hard-coded product values, you will fetch data via an API client. The client does not talk to a real REST API, but simulates one with a heavy latency of 1000ms per request. All tests we've written for this version (`npm test`) will time out after 3000ms, so you'll have to find a smart way deal with multiple requests for that client.

_Hint: Remember that [`async`](https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Statements/async_function) functions such as `fetch#get` just return [Promises](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) under the hood. Maybe there's a built-in way to deal with multiple promises at once._

## Bonus Challenge: Mock the API Client

**This challenge is entirely optional and targeted towards more senior engineering candidates.**

We've built `fetch#get` in a way to throw an error in case a product ID does not exist in its internal "database". In `tests/checkout/v2.test.js` however, we've only tested the happy path so far. We now want to you add the missing bits there to also cover the error case.

_Hint: `src/checkout/v2.js` depends on `src/fetch.js`, but we have not replaced it with a test double for our tests. Luckily, [mocking entire ES6 modules with Jest](https://jestjs.io/docs/es6-class-mocks#automatic-mock) is super simple. To program the mock, you should have a look at [`mockFn.mockResolvedValueOnce(value)`](https://jestjs.io/docs/mock-function-api#mockfnmockresolvedvalueoncevalue) and [`mockFn.mockRejectedValueOnce(value)`](https://jestjs.io/docs/mock-function-api#mockfnmockrejectedvalueoncevalue)._

## Maintainers

* Pascal Cremer (pascal.cremer@schuettflix.de)

## Legal Notes

This case study is intellectual property of [Schüttflix](https://schuettflix.com). Any unauthorized distribution or publication on web, print or other media is strictly prohibited.
