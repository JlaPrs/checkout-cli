import { expect, it, describe } from "@jest/globals";
import * as fetch from '../src/fetch';

describe('fetch#get', () => {
  it('should return correct product', async () => {
    const product = await fetch.get('0003');
    expect(product).toStrictEqual({ id: '0003', name: 'Rindenmulch', price: 250 });
  });

  it('should throw error for unknown product', async () => {
    expect(fetch.get('0815')).rejects.toEqual('Could not find product with id 0815');
  });
});